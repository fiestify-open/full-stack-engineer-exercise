## What you need to know first:
**Welcome!** This is a small exercise that aims to demonstrate your skills in:

 - Javascript 
 - Node.js
 - AngularJS 
 - HTML5 and CSS3 
 - Relational databases 
 - Web Services
 - Offline communication (docs, commits messages, etc.)

It is important that the code is maintainable and well tested using your preferred testing framework. The goal is to automatically validate that the application does what is asked for in the requirements.

## What you're supposed to do:

We want to develop an internal web application for Fiestify employees.

 - **Part 1**: you have to develop a frontend in AngularJS 1.6 with a CSV import option (design is up to you). This frontend must list the CSV file on screen, and it must also communicate with a backend developed in Node.js that is detailed in the second part.
 - **Part 2**: the frontend must interact with a REST API that allows the upload of CSV files for importing certain data into the main database. To do this, the backend in Node.js (v6 or higher) must be able to receive the CSV file, process it and insert the data into a relational database (PostgreSQL is prefered, but could be any SQL database).

## Instructions & How you should submit your exercise:

 1. Clone the Git repository
 2. Work on the exercise
 3. When you finish, along with the code, add the changes to the README.md with the necessary instructions to run the application. 
 4. Make a **Merge Request** with the changes you want to submit when you finish your work.

Thanks for your time!
